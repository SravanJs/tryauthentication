$(document).ready(function () {
	$('#logout').click(function () {
		$.post( "/api/logout")
			.done(function(data) {
				if (data) {
					window.location.href = '/login';
				};
			});
	});
});