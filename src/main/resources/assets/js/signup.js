$(document).ready(function () {
	$('#signup').click(function () {
		if ($('#username').val().trim() !== "" && $('#emailid').val().trim() !== "" && $('#password').val().trim() !== "" && $('#confirm').val().trim() !== "") {
			$.post( "/api/signup", {
				username: $('#username').val(),
				emailid: $('#emailid').val(),
				password: $('#password').val(),
				confirm: $('#confirm').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/login';
				} else {
					$('#message').text(data);
				}
			});
		} else {
			$('#message').text("Please fill all the fields.");
		}
	});
});