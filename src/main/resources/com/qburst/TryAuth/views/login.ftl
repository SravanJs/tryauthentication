<html lang="en">
<head>
	<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<title>Login</title>
</head>
<body>
	<div class="container">
	      <h2 class="col-sm-offset-2">Login</h2>
          <div class="form-horizontal" role="form">
          <div class="form-group">
                <label class="control-label col-sm-2" for="username">Username:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="username" value="" required placeholder="Enter username">
                </div>
          </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="password">Password:</label>
                <div class="col-sm-3">
                    <input type="password" class="form-control" id="password" value="" required placeholder="Enter password">
                </div>
            </div>
            <div class="form-group">
            	<div class="col-sm-offset-2 col-sm-10">
            	<button id="login" class="btn btn-success">Login</button>
            	</div>
            </div>
			<div class="form-group">
            		<div class="col-sm-offset-2 col-sm-10">
            			Yet to Register? <a href="/signup">Sign Up</a><br></br>
            			<font color="red"  size="3">
            			<span id="message" class="col-sm-offset-2" role="alert">${msg?html}</span>
            			</font>
                    </div>
            </div>
        </div>
	</form>
	<script src="public/js/jquery-2.1.3.min.js"></script>
	<script src="public/js/login.js"></script>
</body>
</html>