<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
	<script src="public/js/jquery-2.1.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
    <body>
    <div class="container">
    		<h2 class="col-sm-offset-2">Welcome, ${person.getUsername()?html}</h2>
    		<div>
    		<button id="logout" class="col-sm-offset-2 btn btn-danger">Logout</button>
    		</div>
    </div>
    <script src="public/js/person.js"></script>
    </body>
</html>