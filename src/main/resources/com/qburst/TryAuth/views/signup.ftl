<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sign Up</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
        <h2 class="col-sm-offset-2">Sign Up</h2>
        <div class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="username">Username:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="username" value="" required placeholder="Enter username">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="emailid">Email:</label>
            <div class="col-sm-3">
                <input type="email" class="form-control" id="emailid" value="" required placeholder="Enter email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="password">Password:</label>
            <div class="col-sm-3">
                <input type="password" class="form-control" id="password" value="" required placeholder="Enter password">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="confirm">Confirm Password:</label>
            <div class="col-sm-3">
                <input type="password" class="form-control" id="confirm" value="" required placeholder="Enter password again">
            </div>
        </div>
        <div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button id="signup" button type="submit" class="btn btn-success">Sign Up</button><br></br>
            Already registered? Go to <a href="/login">login</a> page.<br></br>
            <font color="red"  size="3">
            <span id="message" col-sm-offset-2" role="alert">${msg?html}</span>
            </font>
        </div>
		</div>
	</form>
</div>
    <script src="public/js/jquery-2.1.3.min.js"></script>
	<script src="public/js/signup.js"></script>
</body>
</html>
