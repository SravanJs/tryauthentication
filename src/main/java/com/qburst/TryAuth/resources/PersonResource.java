package com.qburst.TryAuth.resources;

import com.qburst.TryAuth.core.Person;
import com.qburst.TryAuth.utils.Redirect;
import com.qburst.TryAuth.views.LoginView;
import com.qburst.TryAuth.views.PersonView;
import com.qburst.TryAuth.views.SignUpView;
import io.dropwizard.jersey.sessions.Session;


import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


/**
 * Created by sravan on 23/4/15.
 */

@Path("/")
public class PersonResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Object getPerson(@Session HttpSession session) {
        if(Redirect.authenticate(session))
            return new PersonView(new Person((String) session.getAttribute("timeid"),(String) session.getAttribute("username")));
        else
            return Redirect.redirectToURI("/login");
    }

    @Path("/login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView getLogin() {
        System.out.println("in get");
        return new LoginView("");
    }

    @Path("/signup")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public SignUpView getJoin() {
        System.out.println("in get");
        return new SignUpView("");
    }

}
