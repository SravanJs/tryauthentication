package com.qburst.TryAuth.resources;

import com.qburst.TryAuth.core.Person;
import com.qburst.TryAuth.dao.PersonDAO;
import com.qburst.TryAuth.utils.Hashing;
import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.regex.Pattern;


/**
 * Created by sravan on 23/4/15.
 */

@Path("/api")
public class ApiResource {

    private PersonDAO personDAO;

    public ApiResource() {};
    public ApiResource(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    @Path("/logout")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.setAttribute("timeid", "");
        session.invalidate();
        return true;
     }

    @Path("/login")
    @POST
    public String postLogin(@FormParam("username") String username, @FormParam("password") String password, @Session HttpSession session) {
        if (username.isEmpty()) { return "Invalid Username"; }
        if (password.isEmpty()) { return "Invalid Password"; }

        Person listLogin = personDAO.getSessionId(username, Hashing.GenerateHash(password));
        if (listLogin!=null) {
            session.setAttribute("username", username);
            session.setAttribute("timeid", listLogin.getTimeid());
            System.out.println("\n\n\n\n" + "true");
            return "true";
        }

        return "ERROR! Incorrect details.";
    }

    @Path("/signup")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public String postJoin(@FormParam("username") String username, @FormParam("emailid") String emailid, @FormParam("password") String password, @FormParam("confirm") String confirm) {
        System.out.println("In post");
        if(username.contains(" ")||emailid.contains(" ")||password.contains(" ")) {
            return "No spaces allowed";
        }
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        if(p.matcher(username).find()) {
            return "No special characters allowed";
        }
        if (username.isEmpty()) {
            return "Username cannot be empty";
        }
        if(!emailid.contains("@")) {
            return "Invalid email id";
        }
        if (emailid.isEmpty()) {
            return "Email cannot be empty";
        }
        if (password.trim().isEmpty()) {
            return "Password cannot be empty";
        }
        if (!password.equals(confirm)) {
            return "Password mismatch";
        }
        if (username.length() > 30) {
            return "Username too long";
        }
        if (emailid.length() > 30) {
            return "Username too long";
        }
        if (password.length() > 30) {
            return "Username too long";
        }

        Person listDuplicate = personDAO.findDuplicate(username.trim());
        if (listDuplicate!=null) {
            return "Username already exists.";
        }

        personDAO.insert(username, emailid, Hashing.GenerateHash(password));
        return "true";
    }
}