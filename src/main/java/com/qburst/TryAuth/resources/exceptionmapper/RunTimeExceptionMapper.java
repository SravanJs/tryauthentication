package com.qburst.TryAuth.resources.exceptionmapper;


import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by sreeraj on 27/4/15.
 */
public class RunTimeExceptionMapper implements ExceptionMapper<RuntimeException> {

    public Response toResponse(RuntimeException e) {
        e.printStackTrace();
        System.out.println(e.getMessage());
        return Response
                .serverError()
                .entity(new com.qburst.TryAuth.views.ErrorView())
                .build();
    }
}