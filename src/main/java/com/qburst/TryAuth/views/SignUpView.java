package com.qburst.TryAuth.views;

import io.dropwizard.views.View;

/**
 * Created by sravan on 24/4/15.
 */
public class SignUpView extends View {

    private final String msg;

    public SignUpView() {
        super("signup.ftl");
        this.msg="";
    }

    public SignUpView(String msg){
        super("signup.ftl");
        this.msg=msg;
    }

    public String getMsg() {
        return msg;
    }


}
