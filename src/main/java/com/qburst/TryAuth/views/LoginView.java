package com.qburst.TryAuth.views;

import io.dropwizard.views.View;

/**
 * Created by sravan on 27/4/15.
 */

public class LoginView extends View {
    private final String msg;

    public LoginView(String msg) {
        super("login.ftl");
        this.msg = msg;
    }

     public String getMsg() {
        return msg;
    }
}
