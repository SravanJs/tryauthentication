package com.qburst.TryAuth.views;

import com.qburst.TryAuth.core.Person;
import io.dropwizard.views.View;

/**
 * Created by sravan on 23/4/15.
 */
public class PersonView extends View {
    private Person person;

    public PersonView(Person person) {
        super("person.ftl");
        this.person = person;
    }

    public Person getPerson() { return person; }
}
