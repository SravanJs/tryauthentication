package com.qburst.TryAuth;

import com.bazaarvoice.dropwizard.assets.ConfiguredAssetsBundle;
import com.datastax.driver.mapping.MappingManager;
import com.qburst.TryAuth.dao.CassandraConn;
import com.qburst.TryAuth.dao.PersonDAO;
import com.qburst.TryAuth.resources.ApiResource;
import com.qburst.TryAuth.resources.PersonResource;
import com.qburst.TryAuth.resources.exceptionmapper.RunTimeExceptionMapper;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;

/**
 * Created by sravan on 22/4/15.
 */

public class TryAuthApplication extends Application<TryAuthConfiguration> {
    public static void main(final String[] args) throws Exception {
            new TryAuthApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<TryAuthConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
        bootstrap.addBundle(new ViewBundle<TryAuthConfiguration>());
        bootstrap.addBundle(new AssetsBundle("/assets", "/public", null,"js"));
        bootstrap.addBundle(new AssetsBundle("/favicon.ico", "/assets/favicon.ico", null, "favicon"));
        bootstrap.addBundle(new ConfiguredAssetsBundle("/assets/", "/dashboard/"));
    }

    @Override
    public void run(TryAuthConfiguration config, Environment environment) {
        MappingManager manager = new MappingManager (new CassandraConn(config.getCassandradb().getHost(),config.getCassandradb().getKeyspace()).getSession());
        PersonDAO personDAO = manager.createAccessor(PersonDAO.class);
        environment.servlets().setSessionHandler(new SessionHandler());
        environment.jersey().register(new PersonResource());
        environment.jersey().register(new ApiResource(personDAO));
        environment.jersey().register((new RunTimeExceptionMapper()));
    }
}