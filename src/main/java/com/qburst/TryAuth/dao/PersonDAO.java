package com.qburst.TryAuth.dao;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.qburst.TryAuth.core.Person;

/**
 * Created by sravan on 5/5/15.
 */

@Accessor
public interface PersonDAO {

    @Query("insert into userdata (timeid , username , emailid, password) values (now(),:username, :emailid, :password)")
    ResultSet insert(@Param("username") String username, @Param("emailid") String emailid, @Param("password") String password);

    @Query("select * from userdata where username = :username AND password = :password allow filtering ")
    Person getSessionId(@Param("username") String username, @Param("password") String password);

    @Query("select * from userdata where username = :username")
    Person findDuplicate(@Param("username") String username);

}

