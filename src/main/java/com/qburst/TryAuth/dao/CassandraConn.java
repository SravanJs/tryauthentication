package com.qburst.TryAuth.dao;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

/**
 * Created by sravan on 5/5/15.
 */
public class CassandraConn {

    private Cluster cluster;
    private Session session;

    public Cluster getCluster() {
        return cluster;
    }

    public Session getSession() {
        return session;
    }

    public CassandraConn(String host, String keyspace)
    {
        cluster = Cluster.builder().addContactPoint(host).build();
        session = cluster.connect(keyspace);
    }

}
