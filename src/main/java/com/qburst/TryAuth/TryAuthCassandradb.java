package com.qburst.TryAuth;


/**
 * Created by sravan on 5/5/15.
 */
public class TryAuthCassandradb {

    private String keyspace;
    private String host;

    public String getKeyspace() {
        return keyspace;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

}
