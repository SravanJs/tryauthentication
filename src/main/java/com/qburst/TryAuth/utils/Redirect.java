package com.qburst.TryAuth.utils;

import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by sravan on 27/4/15.
 */
public class Redirect {
    public static Response redirectToURI(String newURI) {
        URI uri = null;
        try {
            uri = new URI(newURI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Response responseObj = Response.seeOther(uri).build();
        return responseObj;
        //throw new WebApplicationException(responseObj);
    }

    public static boolean authenticate(@Session HttpSession session) {
        String sessionAttribute = ((String) session.getAttribute("username"));
        if (sessionAttribute == null || sessionAttribute.isEmpty()) {
            //Redirect.redirectToURI("/login");
            return false;
        }
        return true;
    }
}
