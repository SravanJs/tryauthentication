package com.qburst.TryAuth.core;

/**
 * Created by sravan on 23/4/15.
 */

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;

@Table(keyspace = "mykeyspace",name = "userdata")
public class Person {

    @PartitionKey
    @NotNull
    @JsonProperty("timeid")
    private String timeid;

    @NotNull
    @JsonProperty("username")
    private String username;

    @NotNull
    @JsonProperty("emailid")
    private String emailid;

    @NotNull
    @JsonProperty("password")
    private String password;

    public String getTimeid() {
        return timeid;
    }

    public void setTimeid(String timeid) {
        this.timeid = timeid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public Person() {}

    public Person(String id,String username) {
        this.timeid =id;
        this.username = username;
    }

    public Person(String username,String emailid,String password){
        this.username=username;
        this.emailid=emailid;
        this.password=password;
    }

    public Person(Person person){
        this.username=person.getUsername();
        this.emailid=person.getEmailid();
        this.password=person.getPassword();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person that = (Person) o;

        if (!getTimeid().equals(that.getTimeid())) return false;
        if (!getUsername().equals(that.getUsername())) return false;
        if (!getEmailid().equals(that.getEmailid())) return false;
        if (!getPassword().equals(that.getPassword())) return false;
        return true;
    }
}

